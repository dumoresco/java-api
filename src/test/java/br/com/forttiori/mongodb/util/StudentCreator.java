package br.com.forttiori.mongodb.util;

import br.com.forttiori.mongodb.v1.model.address.AddressRequest;
import br.com.forttiori.mongodb.v1.model.address.AddressResponse;
import br.com.forttiori.mongodb.v1.model.student.StudentRequest;
import br.com.forttiori.mongodb.v1.model.student.StudentResponse;
import br.com.forttiori.mongodb.v1.model.weather.WeatherResponse;
import br.com.forttiori.mongodb.v1.persistence.entity.*;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.MainEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.SysEntity;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class StudentCreator {

  public static StudentResponse studentResponseStub() {
    return StudentResponse.builder()
        .id("1")
        .firstName("Eduardo")
        .lastName("Moresco")
        .age(20)
        .document("87744163087")
        .email("eduardomorescoiost@gmail.com")
        .address(
            AddressResponse.builder()
                .cep("91740840")
                .logradouro("Rua Joaquim de Carvalho 70")
                .uf("RS")
                .localidade("Porto Alegre")
                .complemento("BLoco 1")
                .bairro("Vila Nova")
                .weather(
                    WeatherEntity.builder()
                        .name("Porto Alegre")
                        .sys(SysEntity.builder().country("RS").build())
                        .main(MainEntity.builder().temp_min(20).temp_max(30).temp(15).build())
                        .build())
                .build())
        .gender(Gender.MALE)
        .startDate(LocalDateTime.now())
        .build();
  }

  public static StudentEntity studentEntityStub1() {
    return StudentEntity.builder()
        .id("2")
        .firstName("Eduardo 1")
        .lastName("Moresco 1")
        .age(20)
        .document("87744163087")
        .email("eduardomorescoiost@gmail.com")
        .address(
            AddressEntity.builder()
                .cep("91740840")
                .bairro("")
                .uf("")
                .logradouro("")
                .localidade("")
                .complemento("")
                .weather(
                    WeatherEntity.builder()
                        .name("Porto Alegre 1")
                        .sys(SysEntity.builder().country("RS 1").build())
                        .main(MainEntity.builder().temp_min(20).temp_max(30).temp(15).build())
                        .build())
                .build())
        .gender(Gender.MALE)
        .startDate(LocalDateTime.now())
        .build();
  }

  public static StudentEntity studentEntityStub2() {
    return StudentEntity.builder()
        .id("3")
        .firstName("Eduardo 2")
        .lastName("Moresco 2")
        .age(20)
        .address(
            AddressEntity.builder()
                .cep("91740840")
                .bairro("")
                .uf("")
                .logradouro("")
                .localidade("")
                .complemento("")
                .weather(
                    WeatherEntity.builder()
                        .name("Porto Alegre 2")
                        .sys(SysEntity.builder().country("RS 2").build())
                        .main(MainEntity.builder().temp_min(20).temp_max(30).temp(15).build())
                        .build())
                .build())
        .document("87744163087")
        .email("eduardomorescoiost@gmail.com")
        .gender(Gender.MALE)
        .startDate(LocalDateTime.now())
        .build();
  }

  public static StudentEntity studentEntityStub3() {
    return StudentEntity.builder()
        .id("4")
        .firstName("Eduardo 3")
        .lastName("Moresco 3")
        .age(20)
        .document("87744163087")
        .email("eduardomorescoiost@gmail.com")
        .address(
            AddressEntity.builder()
                .cep("91740840")
                .bairro("")
                .uf("")
                .logradouro("")
                .localidade("")
                .complemento("")
                .weather(
                    WeatherEntity.builder()
                        .name("Porto Alegre 3")
                        .sys(SysEntity.builder().country("RS 3").build())
                        .main(MainEntity.builder().temp_min(20).temp_max(30).temp(15).build())
                        .build())
                .build())
        .gender(Gender.MALE)
        .startDate(LocalDateTime.now())
        .build();
  }

  public static StudentRequest studentRequestStub() {
    return StudentRequest.builder()
        .firstName("Eduardo 2")
        .lastName("Moresco 2")
        .age(20)
        .document("87744163087")
        .email("eduardomorescoiost@gmail.com")
        .gender(Gender.MALE)
        .address(AddressRequest.builder().cep("91740840").build())
        .startDate(LocalDateTime.now())
        .build();
  }

  public static StudentRequest studentInvalidRequestStub() {
    return StudentRequest.builder()
        .firstName("Eduardo")
        .lastName("Moresco")
        .age(0)
        .document("87744163087")
        .email("eduardomorescoiost@gmail.com")
        .gender(Gender.MALE)
        .address(AddressRequest.builder().cep("91740840").build())
        .startDate(LocalDateTime.now())
        .build();
  }

  public static StudentRequest studentInvalidRequestStub2() {
    return StudentRequest.builder()
        .firstName("Eduardo")
        .lastName("Moresco")
        .age(0)
        .startDate(LocalDateTime.now())
        .build();
  }

  public static AddressResponse addressResponseStub() {
    return AddressResponse.builder()
        .cep("91740840")
        .complemento("aaaa")
        .uf("rs")
        .localidade("porto alegre")
        .logradouro("aaaaa")
        .build();
  }

  public static WeatherResponse weatherResponseStub() {
    return WeatherResponse.builder()
        .name("Porto Alegre 3")
        .sys(SysEntity.builder().country("RS 3").build())
        .main(MainEntity.builder().temp_min(20).temp_max(30).temp(15).build())
        .build();
  }

  public static StudentQuery studentQuery() {
    return StudentQuery.builder().firstName("Eduardo 1").lastName("Moresco 1").build();
  }
}
