package br.com.forttiori.mongodb.service;

import br.com.forttiori.mongodb.util.StudentCreator;
import br.com.forttiori.mongodb.v1.integration.addressIntegration.AddressIntegration;
import br.com.forttiori.mongodb.v1.integration.RestTemplateConfiguration;
import br.com.forttiori.mongodb.v1.integration.weatherIntegration.WeatherIntegration;
import br.com.forttiori.mongodb.v1.model.student.StudentResponse;

import br.com.forttiori.mongodb.v1.persistence.entity.StudentQuery;
import br.com.forttiori.mongodb.v1.persistence.repository.StudentRepository;
import br.com.forttiori.mongodb.v1.service.StudentService;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

  @InjectMocks // Quando quer testar a classe em si, por exemplo a studentService
  StudentService studentService;
  @Mock // Mock do comportamento, vamos definir o comportamento do studentService
  StudentRepository studentRepository;
  @Mock RestTemplate restTemplate;
  @Mock AddressIntegration addressIntegration;
  @Mock RestTemplateConfiguration restTemplateConfiguration;
  @Mock WeatherIntegration weatherIntegration;

  @Test
  @DisplayName("Deve criar um estudante")
  void shouldCreateAnStudent() {

    // Given
    Mockito.when(studentRepository.save(Mockito.any()))
        .thenReturn(StudentCreator.studentEntityStub1());
    Mockito.when(addressIntegration.consultaCep("91740840"))
        .thenReturn(StudentCreator.addressResponseStub());

    // When
    studentService.createStudent(StudentCreator.studentRequestStub());

    // Then
    Assertions.assertEquals("Eduardo 1", StudentCreator.studentEntityStub1().getFirstName());
  }

  @Test
  @DisplayName("Deve retornar uma lista de estudantes")
  void shouldReturnListOfStudents() {

    // Given
    Mockito.when(studentRepository.findAll())
        .thenReturn(
            List.of(
                StudentCreator.studentEntityStub1(),
                StudentCreator.studentEntityStub2(),
                StudentCreator.studentEntityStub3()));

    // When
    List<StudentResponse> studentResponseList = studentService.getAll();

    // Then
    Assertions.assertNotNull(studentResponseList);
    Assertions.assertEquals(3, studentResponseList.size());
    Assertions.assertEquals(StudentResponse.class, studentResponseList.get(0).getClass());
  }

  @Test
  @DisplayName("Deve buscar um estudante pelo id e retorna-lo")
  void shouldReceiveAnIdThenReturnCorrespondentUser() {

    // Given
    Mockito.when(studentRepository.findById(Mockito.anyString()))
        .thenReturn(Optional.ofNullable(StudentCreator.studentEntityStub1()));
    // When
    StudentResponse studentResponse = studentService.getStudentsById("2");
    // Then
    Assertions.assertNotNull(studentResponse);
    Assertions.assertEquals(StudentResponse.class, studentResponse.getClass());
    Assertions.assertEquals(StudentCreator.studentEntityStub1().getId(), studentResponse.getId());
  }

  @Test
  @DisplayName("Deve deletar a lista de estudantes especificadas.")
  void shouldReceiveAListOfIdsAndDeleteSpecifiedUsers() {
    //      GIVEN
    Mockito.when(studentRepository.findById(Mockito.anyString()))
        .thenReturn(Optional.ofNullable((StudentCreator.studentEntityStub1())));
    //      WHEN
    studentService.deleteAll(
        List.of(
            StudentCreator.studentEntityStub1().getId(),
            StudentCreator.studentEntityStub2().getId()));
    //      THEN
    Mockito.verify(studentRepository, Mockito.times(1))
        .deleteAllById(
            List.of(
                StudentCreator.studentEntityStub1().getId(),
                StudentCreator.studentEntityStub2().getId()));
  }

  @Test
  @DisplayName("Deve deletar todos os estudantes")
  void shouldReceiveAnEmptyListAndDeleteAllStudents() {
    //      GIVEN
    Mockito.doNothing().when(studentRepository).deleteAll();
    //      WHEN
    studentService.deleteAll(null);
    //      THEN
    Mockito.verify(studentRepository, Mockito.times(1)).deleteAll();
  }

  @Test
  @DisplayName("Deve fazer o update do estudante.")
  void shoulUpdateAnSpecifiedUser() {
    //      GIVEN
    Mockito.when(studentRepository.findById(Mockito.anyString()))
        .thenReturn(Optional.ofNullable(StudentCreator.studentEntityStub1()));
    Mockito.when(addressIntegration.consultaCep("91740840"))
        .thenReturn(StudentCreator.addressResponseStub());
    Mockito.when(studentRepository.save(Mockito.any()))
        .thenReturn(StudentCreator.studentEntityStub1());
    Mockito.when(weatherIntegration.getWeather("Porto Alegre")).thenReturn(StudentCreator.weatherResponseStub());

    //      WHEN
    studentService.updateStudent(
        StudentCreator.studentEntityStub1().getId(), StudentCreator.studentRequestStub());
    //      THEN
    Assertions.assertNotNull(StudentCreator.studentEntityStub1());
  }

  @Test
  @DisplayName("Deve retornar lista de estudantes filtrados")
  void ShouldReturnAnListOfStudentsFiltered() {

    StudentQuery studentQuery = new StudentQuery();

    Mockito.when(studentRepository.find(studentQuery.getFirstName(), studentQuery.getLastName()))
        .thenReturn(
            List.of(StudentCreator.studentEntityStub1(), StudentCreator.studentEntityStub2()));
    studentService.findStudentByName(studentQuery);
    Assertions.assertEquals("", studentQuery.getFirstName());
  }
}
