package br.com.forttiori.mongodb.integration;

import br.com.forttiori.mongodb.util.StudentCreator;
import br.com.forttiori.mongodb.v1.model.student.StudentResponse;
import br.com.forttiori.mongodb.v1.persistence.entity.StudentEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.StudentQuery;
import br.com.forttiori.mongodb.v1.persistence.repository.StudentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class StudentControllerIntegrationTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private StudentRepository studentRepository;

  @Autowired private ObjectMapper objectMapper;

  @BeforeEach // Preenche o banco de dados com dados do stub antes de todos os testes
  void startStubs() {
    studentRepository.save(StudentCreator.studentEntityStub1());
    studentRepository.save(StudentCreator.studentEntityStub2());
    studentRepository.save(StudentCreator.studentEntityStub3());
  }

  @AfterEach
  void deleteStubs() {
    studentRepository.deleteAll();
  }

  @Test
  @DisplayName("Should return a list of all students")
  void shouldReturnAllStudentsWhenSuccess() throws Exception {

    // When
    // perform(): Perform a request and return a type that allows chaining further actions, such as asserting expectations, on the result.
    // get(): Create a MockHttpServletRequestBuilder for a GET request.
    MvcResult mvcResult =
        mockMvc.perform(get("/v1/students")).andDo(print()).andExpect(status().isOk()).andReturn();

    // getResponse(): Return the resulting response.
    // getContentAsString(): Get the content of the response body as a String
    String json = mvcResult.getResponse().getContentAsString();
    // readValue(): Method to deserialize JSON content from given JSON content String.
    StudentResponse[] productResponses = objectMapper.readValue(json, StudentResponse[].class);

    // Then
    // assertThat(): receive actual value and return the assertion object
    assertThat(productResponses).isInstanceOf(StudentResponse[].class).isNotNull().hasSize(3);
  }

  @Test
  @DisplayName("Should return a pageable list of all users")
  void shouldReturnAPageableOfAllStudents() throws Exception {
    MvcResult mvcResult =
        mockMvc
            .perform(get("/v1/students/pages"))
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn();

    String json = mvcResult.getResponse().getContentAsString();

    // Then
    assertThat(json).isNotNull().isNotEmpty();
  }

  @Test
  @DisplayName("Should receive an ID and return a respective student")
  void shouldReceiveAnIdAndReturnRespectiveStudent() throws Exception {

    // When
    MvcResult mvcResult =
        mockMvc
            .perform(get("/v1/students/2"))
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn();

    Optional<StudentEntity> studentEntityOptional = studentRepository.findById("2");
    // Then
    assertThat(studentEntityOptional).isNotEmpty();
    assertThat(studentEntityOptional.get().getFirstName()).contains("Eduardo 1");
  }

  @Test
  @DisplayName("Should receive an filter and return respective students")
  void shouldReceiveAnFilterAndReturnRespectiveStudents() throws Exception {
    // Given
    StudentQuery studentQuery = StudentCreator.studentQuery();
    // When
    MvcResult mvcResult =
        mockMvc
            .perform(
                get("/v1/students/filter")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(studentQuery)))
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn();
    String json = mvcResult.getResponse().getContentAsString();
    StudentResponse[] productResponses = objectMapper.readValue(json, StudentResponse[].class);

    // Then
    assertThat(productResponses).isNotEmpty().isNotNull().hasSize(1);
    assertThat(Arrays.stream(productResponses).toList().get(0).getFirstName())
        .isEqualTo(studentQuery.getFirstName());
  }

  @Test
  @DisplayName("Should create an student")
  void shouldCreateAnStudent() throws Exception {
    String request = objectMapper.writeValueAsString(StudentCreator.studentRequestStub());

    MvcResult mvcResult =
        mockMvc
            .perform(post("/v1/students").contentType(MediaType.APPLICATION_JSON).content(request))
            .andExpect(status().isCreated())
            .andReturn();
  }

  @Test
  @DisplayName("Should delete an student by id")
  void shouldDeleteAnStudentById() throws Exception {
    MvcResult mvcResult =
        mockMvc
            .perform(delete("/v1/students").param("id", "2"))
            .andExpect(status().isNoContent())
            .andReturn();

    assertThat(studentRepository.findById("2")).isEmpty();
    assertThat(studentRepository.findAll()).hasSize(2);
  }

  @Test
  @DisplayName("Should delete all students")
  void shouldDeleteAllStudents() throws Exception {
    MvcResult mvcResult =
        mockMvc.perform(delete("/v1/students")).andExpect(status().isNoContent()).andReturn();

    assertThat(studentRepository.findById("2")).isEmpty();
    assertThat(studentRepository.findById("1")).isEmpty();
    assertThat(studentRepository.findById("3")).isEmpty();
    assertThat(studentRepository.findAll()).isEmpty();
  }

  @Test
  @DisplayName("Should update an student")
  void shouldUpdateAnStudent() throws Exception {
    String request = objectMapper.writeValueAsString(StudentCreator.studentRequestStub());

    MvcResult mvcResult =
        mockMvc
            .perform(put("/v1/students/3").contentType(MediaType.APPLICATION_JSON).content(request))
            .andExpect(status().isAccepted())
            .andReturn();

    Optional<StudentEntity> studentEntityOptional = studentRepository.findById("3");

    assertThat(studentEntityOptional).isNotEmpty();
    assertThat(studentEntityOptional.get().getFirstName()).contains("Eduardo 2");
  }

  @Test
  void createInvalidStudentReturnException() throws Exception {
    String request = objectMapper.writeValueAsString(StudentCreator.studentInvalidRequestStub());

    MvcResult mvcResult =
        mockMvc
            .perform(post("/v1/students").contentType(MediaType.APPLICATION_JSON).content(request))
            .andExpect(status().isBadRequest())
            .andReturn();

    assertThat(mvcResult.getResponse().getContentAsString()).contains("Age must be greater than 0");
  }

  @Test
  void serverInternalErrorException() throws Exception {
    String request = """
    {
    	"DASD": "Lucas Castro",
    	"dasd": "dasd",
    	"agedasd" : 10,
    	"gedasnder": 
    	"emdsail" : "lucasasdsacastro@gmail.com",
    	"dodscument": "877sad44163087",
    	"cdsdsaep": "212dsad12121"
    }
    """;

    MvcResult mvcResult =
            mockMvc
                    .perform(post("/v1/students").contentType(MediaType.APPLICATION_JSON).content(request))
                    .andExpect(status().isInternalServerError())
                    .andReturn();

    assertThat(mvcResult.getResponse().getContentAsString()).contains("Something's Wrong");

  }

  @Test
  void findInvalidIDReturnException() throws Exception {

    MvcResult mvcResult =
        mockMvc.perform(get("/v1/students/sadasd")).andExpect(status().isNotFound()).andReturn();
  }

  @Test
  void shouldReturnAnWeatherOfCity() throws Exception {
    MvcResult mvcResult =
            mockMvc
                    .perform(get("/v1/students/weather").param("q", "Porto Alegre"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andReturn();

    String json = mvcResult.getResponse().getContentAsString();

    // Then
    assertThat(json).isNotNull().isNotEmpty().contains("name");
  }
}
