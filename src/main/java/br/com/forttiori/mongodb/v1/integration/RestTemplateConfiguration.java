package br.com.forttiori.mongodb.v1.integration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

  @Bean
  public RestTemplate cepRestTemplate() {
    return new RestTemplateBuilder()
        .rootUri("https://viacep.com.br")
        .defaultHeader("Content-Type", String.valueOf(MediaType.APPLICATION_JSON))
        .errorHandler(new DefaultResponseErrorHandler())
        .build();
  }

  @Bean
  public RestTemplate weatherRestTemplate() {
    return new RestTemplateBuilder()
        .rootUri("https://api.openweathermap.org/data/2.5")
        .errorHandler(new DefaultResponseErrorHandler())
        .defaultHeader("Content-Type", String.valueOf(MediaType.APPLICATION_JSON))
        .build();
  }
}
