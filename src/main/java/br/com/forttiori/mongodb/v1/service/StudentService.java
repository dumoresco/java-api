package br.com.forttiori.mongodb.v1.service;

import br.com.forttiori.mongodb.v1.exceptions.exceptionmodels.EntityNotFoundException;
import br.com.forttiori.mongodb.v1.integration.addressIntegration.AddressIntegration;
import br.com.forttiori.mongodb.v1.integration.weatherIntegration.WeatherIntegration;
import br.com.forttiori.mongodb.v1.model.address.AddressRequest;
import br.com.forttiori.mongodb.v1.model.address.AddressResponse;
import br.com.forttiori.mongodb.v1.model.student.StudentRequest;
import br.com.forttiori.mongodb.v1.model.student.StudentResponse;
import br.com.forttiori.mongodb.v1.model.student.mapper.RequestMapper;
import br.com.forttiori.mongodb.v1.model.student.mapper.ResponseMapper;
import br.com.forttiori.mongodb.v1.model.weather.WeatherResponse;
import br.com.forttiori.mongodb.v1.persistence.entity.StudentEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.StudentQuery;
import br.com.forttiori.mongodb.v1.persistence.repository.StudentRepository;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

@Service
public class StudentService {
  @Autowired StudentRepository studentRepository;

  @Autowired AddressIntegration addressIntegration;

  @Autowired WeatherIntegration weatherIntegration;

  // Refatorar para retornar StudentResponse
  public List<StudentResponse> getAll() {
    return studentRepository.findAll().stream().map(ResponseMapper::createResponse).toList();
  }

  public Page<StudentResponse> getStudentsByPage(Pageable pageable) {
    return studentRepository.findAll(pageable).map(ResponseMapper::createResponse);
  }

  public StudentResponse getStudentsById(String id) {
    var getById =
        studentRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException(" Id not found: " + id));
    return ResponseMapper.createResponse(getById);
  }

  public void createStudent(@Valid @NotNull StudentRequest studentRequest) {
    AddressResponse addressResponse =
        addressIntegration.consultaCep(studentRequest.getAddress().getCep());
    WeatherResponse weatherResponse = getWeather(addressResponse.getLocalidade());

    StudentEntity students =
        RequestMapper.createEntity(studentRequest, addressResponse, weatherResponse);

    studentRepository.save(students);
  }

  public List<StudentResponse> findStudentByName(@NotNull StudentQuery studentQuery) {
    String firstName = studentQuery.getFirstName();
    String lastName = studentQuery.getLastName();

    return studentRepository.find(firstName, lastName).stream()
        .map(ResponseMapper::createResponse)
        .toList();
  }

  public void deleteAll(List<String> id) {
    if (id == null) {
      studentRepository.deleteAll();
    } else {
      for (String i : id) {
        getStudentsById(i);
      }
      studentRepository.deleteAllById(id);
    }
  }
  // Método para atualizar um estudante
  public void updateStudent(String id, StudentRequest request) {
    AddressResponse addressResponse =
            addressIntegration.consultaCep(request.getAddress().getCep());

    WeatherResponse weatherResponse1 = getWeather(addressResponse.getLocalidade());

    StudentEntity oldStudentEntity =
        studentRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException(" Id not found: " + id));

    StudentEntity newStudentEntity =
        RequestMapper.createEntity(
            request,
            addressIntegration.consultaCep(request.getAddress().getCep()),
                weatherResponse1);

    newStudentEntity.setId(oldStudentEntity.getId());

    ResponseMapper.createResponse(studentRepository.save(newStudentEntity));
  }

  public WeatherResponse getWeather(String city) {
    WeatherResponse weatherResponse = weatherIntegration.getWeather(city);

    double temp = weatherResponse.getMain().getTemp();
    double temp_min = weatherResponse.getMain().getTemp_min();
    double temp_max = weatherResponse.getMain().getTemp_max();

    int tempCalc = (int) (temp - 273.15);
    int temp_minCalc = (int) (temp_min - 273.15);
    int temp_maxCalc = (int) (temp_max - 273.15);

    weatherResponse.getMain().setTemp(tempCalc);
    weatherResponse.getMain().setTemp_min(temp_minCalc);
    weatherResponse.getMain().setTemp_max(temp_maxCalc);

    return weatherResponse;
  }

  // Esse método vai buscar o cep na api e retornar a classe com os dados injetados

}
