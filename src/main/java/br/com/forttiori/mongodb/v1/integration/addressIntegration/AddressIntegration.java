package br.com.forttiori.mongodb.v1.integration.addressIntegration;

import br.com.forttiori.mongodb.v1.model.address.AddressResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AddressIntegration {

  @Qualifier("cepRestTemplate")
  @Autowired
  RestTemplate cepRestTemplate;


  public AddressResponse consultaCep(String cep) {
    var URL = "/ws/" + cep + "/json/";

    return cepRestTemplate.getForObject(URL, AddressResponse.class);
  }
}
