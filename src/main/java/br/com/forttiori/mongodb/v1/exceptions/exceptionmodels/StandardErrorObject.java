package br.com.forttiori.mongodb.v1.exceptions.exceptionmodels;

import lombok.*;

@Getter
@AllArgsConstructor
@Builder
public class StandardErrorObject {

    String message;
    String field;

}
