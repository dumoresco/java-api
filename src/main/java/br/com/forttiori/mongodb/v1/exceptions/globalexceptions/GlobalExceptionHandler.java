package br.com.forttiori.mongodb.v1.exceptions.globalexceptions;

import br.com.forttiori.mongodb.v1.exceptions.exceptionmodels.EntityNotFoundException;
import br.com.forttiori.mongodb.v1.exceptions.exceptionmodels.StandardErrorObject;
import br.com.forttiori.mongodb.v1.exceptions.exceptionmodels.StandardErrorResponse;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(code = NOT_FOUND)
  public StandardErrorResponse handleNotFoundException(EntityNotFoundException e) {
    return (StandardErrorResponse.builder()
        .name("Id not Found")
        .errorObjects(
            List.of(
                StandardErrorObject.builder()
                    .message(NOT_FOUND.name())
                    .field((e.getMessage()))
                    .build()))
        .build());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(code = BAD_REQUEST)
  public StandardErrorResponse handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
    List<FieldError> errorList = e.getBindingResult().getFieldErrors();
    return (StandardErrorResponse.builder()
        .name("Validation Exception")
        .errorObjects(
            errorList.stream()
                .map(
                    fieldError ->
                        StandardErrorObject.builder()
                            .field(fieldError.getField())
                            .message(fieldError.getDefaultMessage())
                            .build())
                .toList())
        .build());
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(code = INTERNAL_SERVER_ERROR)
  public StandardErrorResponse handleGenericException(Exception e) {
    return StandardErrorResponse.builder()
        .name("Something's Wrong")
        .errorObjects(
            List.of(
                StandardErrorObject.builder()
                    .message(INTERNAL_SERVER_ERROR.name())
                    .field(e.getMessage())
                    .build()))
        .build();
  }
}
