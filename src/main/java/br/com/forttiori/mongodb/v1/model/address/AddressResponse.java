package br.com.forttiori.mongodb.v1.model.address;

import br.com.forttiori.mongodb.v1.model.weather.WeatherResponse;
import br.com.forttiori.mongodb.v1.persistence.entity.WeatherEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressResponse {
    private String cep;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String localidade;
    private String uf;
    private WeatherEntity weather;
}
