package br.com.forttiori.mongodb.v1.integration.weatherIntegration;

import br.com.forttiori.mongodb.v1.model.weather.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherIntegration {

    @Qualifier("weatherRestTemplate")
    @Autowired
    RestTemplate weatherRestTemplate;



    public WeatherResponse getWeather(String city){
        return weatherRestTemplate.getForObject("/weather?q=" + city + "&appid=3b1111b30078451741d147ccef600736", WeatherResponse.class);
    }
}
