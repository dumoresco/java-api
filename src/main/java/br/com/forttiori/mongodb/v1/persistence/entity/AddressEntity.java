package br.com.forttiori.mongodb.v1.persistence.entity;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class AddressEntity {

        private String cep;
        private String logradouro;
        private String complemento;
        private String bairro;
        private String localidade;
        private String uf;
        private WeatherEntity weather;
}
