package br.com.forttiori.mongodb.v1.model.student.mapper;

import br.com.forttiori.mongodb.v1.model.address.AddressResponse;
import br.com.forttiori.mongodb.v1.model.student.StudentRequest;
import br.com.forttiori.mongodb.v1.model.weather.WeatherResponse;
import br.com.forttiori.mongodb.v1.persistence.entity.AddressEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.StudentEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.WeatherEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.MainEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.SysEntity;

import java.time.LocalDateTime;

public class RequestMapper {
  public static StudentEntity createEntity(
      StudentRequest studentRequest,
      AddressResponse addressResponse,
      WeatherResponse weatherResponse) {
    return StudentEntity.builder()
        .firstName(studentRequest.getFirstName())
        .lastName(studentRequest.getLastName())
        .age(studentRequest.getAge())
        .email(studentRequest.getEmail())
        .document(studentRequest.getDocument())
        .gender(studentRequest.getGender())
        .address(
            AddressEntity.builder()
                .cep(addressResponse.getCep())
                .complemento(addressResponse.getComplemento())
                .localidade(addressResponse.getLocalidade())
                .logradouro(addressResponse.getLogradouro())
                .uf(addressResponse.getUf())
                .bairro(addressResponse.getBairro())
                .weather(
                    WeatherEntity.builder()
                        .name(weatherResponse.getName())
                        .main(
                            MainEntity.builder()
                                .temp(weatherResponse.getMain().getTemp())
                                .temp_max(weatherResponse.getMain().getTemp_max())
                                .temp_min(weatherResponse.getMain().getTemp_min())
                                .build())
                        .sys(
                            SysEntity.builder()
                                .country(weatherResponse.getSys().getCountry())
                                .build())
                        .build())
                .build())
        .startDate(LocalDateTime.now())
        .build();
  }
}
