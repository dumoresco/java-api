package br.com.forttiori.mongodb.v1.model.weather;

import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.MainEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.SysEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WeatherResponse {
    private String name;
    private SysEntity sys;
    private MainEntity main;
}
