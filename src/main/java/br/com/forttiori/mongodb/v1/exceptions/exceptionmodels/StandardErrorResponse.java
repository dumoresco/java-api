package br.com.forttiori.mongodb.v1.exceptions.exceptionmodels;

import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
public class StandardErrorResponse {

    String name;
    List<StandardErrorObject> errorObjects;

}
