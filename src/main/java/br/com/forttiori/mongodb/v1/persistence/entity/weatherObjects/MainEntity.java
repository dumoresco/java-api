package br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MainEntity {
    private int temp;
    private int temp_min;
    private int temp_max;
}
