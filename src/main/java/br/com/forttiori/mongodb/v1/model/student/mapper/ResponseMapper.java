package br.com.forttiori.mongodb.v1.model.student.mapper;

import br.com.forttiori.mongodb.v1.model.address.AddressResponse;
import br.com.forttiori.mongodb.v1.model.student.StudentResponse;
import br.com.forttiori.mongodb.v1.model.weather.WeatherResponse;
import br.com.forttiori.mongodb.v1.persistence.entity.StudentEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.WeatherEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.MainEntity;
import br.com.forttiori.mongodb.v1.persistence.entity.weatherObjects.SysEntity;

public class ResponseMapper {
  public static StudentResponse createResponse(StudentEntity student) {
    return StudentResponse.builder()
        .id(student.getId())
        .firstName(student.getFirstName())
        .lastName(student.getLastName())
        .age(student.getAge())
        .gender(student.getGender())
        .email(student.getEmail())
        .gender(student.getGender())
        .document(student.getDocument())
        .address(
            AddressResponse.builder()
                .cep(student.getAddress().getCep())
                .complemento(student.getAddress().getComplemento())
                .bairro(student.getAddress().getBairro())
                .localidade(student.getAddress().getLocalidade())
                .uf(student.getAddress().getUf())
                .logradouro(student.getAddress().getLogradouro())
                .weather(
                    WeatherEntity.builder()
                        .name(student.getAddress().getWeather().getName())
                        .main(
                            MainEntity.builder()
                                .temp(student.getAddress().getWeather().getMain().getTemp())
                                .temp_max(student.getAddress().getWeather().getMain().getTemp_max())
                                .temp_min(student.getAddress().getWeather().getMain().getTemp_min())
                                .build())
                        .sys(
                            SysEntity.builder()
                                .country(student.getAddress().getWeather().getSys().getCountry())
                                .build())
                        .build())
                .build())
        .startDate(student.getStartDate())
        .build();
  }
}
