package br.com.forttiori.mongodb.v1.exceptions.exceptionmodels;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String message) {
        super(message);
    }
}
